@echo off

setlocal
set PATH=%PATH%;c:\python27
set PYTHONPATH=python

python python\test\ThrottleControllerTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

python python\test\TurboControllerTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

python python\test\LaneControllerTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

python python\test\LaneSwitcherSimpleTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

python python\test\RaceTrackTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

python python\test\LineEqTest.py
if %errorlevel% neq 0 exit /b %errorlevel%

endlocal
