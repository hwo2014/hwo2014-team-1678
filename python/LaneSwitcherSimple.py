from LaneSwitcher import LaneSwitcher

class LaneSwitcherSimple(LaneSwitcher):

    def __init__(self, raceTrack):
        LaneSwitcher.__init__(self, raceTrack)
        self.numTrackSections = raceTrack.number_of_track_sections()
        self.bestLanes        = self.get_best_lane_indexes()
        #self.numTrackSections = len(self.bestLanes)

    def get_best_lane_indexes(self):
        laneLengths = self.get_lane_lengths()
        bestLanes = []
        shortestSections = map(lambda section: min(section), laneLengths)
        for idx, section in enumerate(shortestSections):
            bestLanes.append(laneLengths[idx].index(shortestSections[idx]))
        return bestLanes

    #    return a list of size [track lanes x track sections]
    #    elements are lengths between consecutive track switches
    # [10, 20, 15, ...
    #  15, 15, 15, ...
    #  20, 10, 15, ...
    #  25, 5,  15, ...]
    def get_lane_lengths(self):
        laneLengths  = []
        numOfLanes   = self.raceTrack.number_of_lanes()
        sectionLen   = [0]*numOfLanes
        sectionIndex = 0
        for pieceIdx, piece in enumerate(self.raceTrack.track_pieces):
            if piece['switch'] > 0:
                laneLengths.append(sectionLen)
                sectionLen = [0]*numOfLanes
                sectionIndex = (sectionIndex + 1) % self.numTrackSections
            sectionLen = map(sum, zip(sectionLen, (self.get_lane_lengths_in(piece))))
            self.trackPieceSectionIndex[pieceIdx] = sectionIndex
        if sectionLen is not []:
            laneLengths.append(sectionLen)

        laneLengths = self.combine_track_beginning_and_end_sections(laneLengths)
        return laneLengths

    def get_lane_lengths_in(self, trackPiece):
        sectionLengths = []
        for lane in self.raceTrack.track_lanes:
            if int(trackPiece['radius']) > 0:
                sectionLengths.append(abs(trackPiece['angle']) / 360 * self.get_lane_radius(trackPiece, lane))
            else:
                sectionLengths.append(trackPiece['length'])
        return sectionLengths

    def get_lane_radius(self, trackPiece, lane):
        radius       = trackPiece['radius']
        distFromCntr = lane['distanceFromCenter']
        if trackPiece['angle'] > 0:
            radius = radius - distFromCntr
        else:
            radius = radius + distFromCntr
        return radius

    def get_best_lane_for_section(self, sectionIndex):
        return self.bestLanes[sectionIndex % self.numTrackSections]

    def track_piece_section_index(self, pieceIndex):
        return self.trackPieceSectionIndex[pieceIndex]

    def combine_track_beginning_and_end_sections(self, laneLengths):
        if len(laneLengths) > 2: # More than two track sections
            laneLengths[0] = map(sum, zip(laneLengths[0], laneLengths[-1]))
            del laneLengths[-1]
        return laneLengths
