﻿def safeInv(x):
    try:
        return 1.0/float(x)
    except ZeroDivisionError:
        return float('inf') if x > 0 else float('-inf') if x < 0 else float('nan')
    except:
        return float('nan')

def safeDiv(numer, denom):
    return numer * safeInv(denom)

def slope(x1, y1, x2, y2):
    return safeDiv(y2 - y1, x2 - x1)

def slopeAndIntercepts(x1, y1, x2, y2):
    m  = slope(x1, y1, x2, y2)
    m_ = slope(y1, x1, y2, x2)
    x  = x1 - m_*y1
    y  = y1 -  m*x1
    return m, x, y
