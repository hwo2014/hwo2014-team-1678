import math
from collections import deque

import copy

import Misc
from TrackPhysics import Drift

MAX_THROTTLE         = 1.0
MEASUREMENT_THROTTLE = 1.0
CONSTANT_THROTTLE    = 0.64
MIN_THROTTLE         = 0.0

ANGLE_LIMIT = 15
INITIAL_THROTTLE = 0.64
THROTTLE_DECREASE = 0.5
MAX_SPEED_INCREASE = 2
SPEED_WINDOW_SIZE = 20

STRATEGY_MEASURE = 0
STRATEGY_RACE    = 1

CRASH_ANGLE = 40.0 # 60.0 is actual crash threshold
LOOKAHEAD_ACCELERATE_TIMESTEPS = 15
LOOKAHEAD_BRAKE_TIMESTEPS      = 30

class Timestep(object):

    def __init__(self, position, speed, angle, omega, accelPhys, driftPhys, raceTrack):
        self.accelPhys = accelPhys
        self.driftPhys = Drift(driftPhys.sigmas)
        self.raceTrack = raceTrack

        self.position  = position
        self.speed     = speed
        self.angle     = angle
        self.omega     = omega

    def simulate(self, throttle):
        accel = throttle*self.accelPhys.y + self.accelPhys.m*self.speed
        self.speed += accel
        self.position.advance(self.speed, self.raceTrack)
        self.omega = Misc.calcOmega(self.position.radius, self.position.angle, self.speed)
        self.driftPhys.update(self.omega)
        self.angle = self.driftPhys.getDriftEstimate(self.speed)

class ThrottleController(object):

    def __init__(self, raceTrack, carState, accelPhys, driftPhys):
        self.raceTrack      = raceTrack
        self.carState       = carState
        self.accelPhys      = accelPhys
        self.driftPhys      = driftPhys
        self.strategy       = STRATEGY_MEASURE
        self._cur_throttle = INITIAL_THROTTLE
        self._speed_window = deque(maxlen=SPEED_WINDOW_SIZE)

        # just for debug purposes
        self.predictedAngle1 = 0.0
        self.predictedSpeed1 = 0.0
        self.predictedAngle2 = 0.0
        self.predictedSpeed2 = 0.0

    def get_throttle(self):
        if STRATEGY_MEASURE == self.strategy:
            return MEASUREMENT_THROTTLE

        return self.estimate_throttle()
        """
        Args:
            angle: Car angle as float.
            turbo_factor: Turbo factor as float.
            speed: Car speed as float.
        """
        self._speed_window.append(speed)
        avg_speed = sum(self._speed_window) / len(self._speed_window)
        if math.fabs(angle) < ANGLE_LIMIT:
            self._cur_throttle += (1-self._cur_throttle) / (100*turbo_factor)
            self._cur_throttle = min(1.0, self._cur_throttle)
        else:
            self._cur_throttle -= THROTTLE_DECREASE * turbo_factor
            self._cur_throttle = max(0.0, self._cur_throttle)
        if (speed - avg_speed) > MAX_SPEED_INCREASE:
            self._cur_throttle /= 2
        """
        print 'Angle: %d, Speed: %0.2f, Avg. Speed: %0.2f, Throttle: %0.2f' % (
            math.fabs(angle), speed, avg_speed, self._cur_throttle)
        """

    def go_nuts(self):
        self.strategy = STRATEGY_RACE

    def estimate_throttle(self):
        throttle = MAX_THROTTLE
        if self.will_crash(throttle):
            return MIN_THROTTLE
        else:
            return throttle

    def will_crash(self, throttle):
        s = self.carState
        position = copy.copy(s.locations[-1]) # better not be a dummy location
        dt = Timestep(position, s.speed, s.angle, s.omega,
                      self.accelPhys, self.driftPhys, self.raceTrack)
        try:
            for i in xrange(LOOKAHEAD_ACCELERATE_TIMESTEPS):
                dt.simulate(throttle)
                if abs(dt.angle) >= CRASH_ANGLE:
                    return True
        finally:
            self.predictedAngle1 = dt.angle
            self.predictedSpeed1 = dt.speed
        try:
            for i in xrange(LOOKAHEAD_BRAKE_TIMESTEPS):
                dt.simulate(MIN_THROTTLE)
                if abs(dt.angle) >= CRASH_ANGLE:
                    return True
            return False
        finally:
            self.predictedAngle2 = dt.angle
            self.predictedSpeed2 = dt.speed
