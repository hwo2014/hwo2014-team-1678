
import dijkstra
import re

class LaneControllerGraph(object):

    def __init__(self, raceTrack):
        self.raceTrack = raceTrack
        self.numOfPieces = raceTrack.number_of_track_pieces
        self.numOfLanes  = raceTrack.number_of_lanes()

    def shortest_path(self, raceTrackGraphSection, startNode, lookAhead):
        endNodes = self.derive_end_nodes(startNode, lookAhead, self.numOfLanes)
        [dist, prev] = dijkstra.shortest_path(raceTrackGraphSection, startNode, endNodes)
        endNodesDistances = map(lambda node: dist[node], endNodes)
        minIdx = endNodesDistances.index(min(endNodesDistances))
        closestEndNode = endNodes[minIdx]
        shortestPath = {closestEndNode: prev[closestEndNode]}
        node = prev[closestEndNode]
        previousNode = closestEndNode
        while node != startNode:
            shortestPath[prev[node]] = previousNode
            #print("Printing path {0}: {1}".format(prev[node], previousNode))
            previousNode = prev[node]
            node = prev[node]

        #return dijkstra.reverse_prev_list(prev)
        return shortestPath

    def derive_end_nodes(self, startNode, lookAhead, numOfLanes):
        currentPiece = re.match(r"p([0-9]+)+l([0-9]+)", startNode)
        endPieces = []
        for lane in xrange(numOfLanes):
            endPieces.append('p'+`(int(currentPiece.group(1))+lookAhead)%self.numOfPieces`+'l'+`lane`)
        return endPieces