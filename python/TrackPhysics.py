﻿import collections

import LineEq

# acceleration = m*speed + y
# m does not vary with throttle, but varies between races and/or tracks
# x and y depend linearly on throttle, these values are normalized to throttle 1.0

DEFAULT_M = -0.020 # friction, drag, engine power, mass ?
DEFAULT_X = 10.0   # max speed
DEFAULT_Y = 0.20   # max acceleration

class Acceleration(object):

    def __init__(self):
        self.m = DEFAULT_M
        self.x = DEFAULT_X
        self.y = DEFAULT_Y

    def update(self, m, x, y, throttle):
        self.m = m
        self.x = LineEq.safeDiv(x, throttle)
        self.y = LineEq.safeDiv(y, throttle)
        print 'Acceleration update:', self.m, self.x, self.y, throttle


# Car drifting angle estimation:
# y: sigma, estimated drifting angle / speed
# x: angular speed, omega
# y[n] = A*x[n] + B*y[n-1] + C*y[n-2]

B =  1.9130
C = -0.9209

# Optimal value might depend on track/server physics.
# Increase to overestimate --> increase safety margin.
# Track physics adaptation should reset this value based on some measurements.
A =  0.012 # 0.085

FILTER_DEPTH = 3

class Drift(object):

    def __init__(self, initial=collections.deque(FILTER_DEPTH*(0.0, ), maxlen=FILTER_DEPTH)):
        self.sigmas = collections.deque(initial, maxlen=FILTER_DEPTH)

    def update(self, omega):
        s = A*omega + B*self.sigmas[-1] + C*self.sigmas[-2]
        self.sigmas.append(s)
        return s

    def getDriftEstimate(self, speed):
        return self.sigmas[-1]*speed
