class TurboController(object):

    def __init__(self, laneController, raceTrack):
        self.laneController = laneController
        self.raceTrack      = raceTrack
        self.turboAvailable = False
        self.turboDetails = {'turboDurationMilliseconds': 0,
                             'turboDurationTicks': 0,
                             'turboFactor': 0}
        self.currentPosition = {}

    def turbo_available(self, turboMsg):
        self.turboAvailable = True
        self.turboDetails   = turboMsg

    def use_turbo_now(self, carPositionMsg):
        return False # DEBUG
        self.currentPosition = carPositionMsg
        if self.turboAvailable:
            if self.nothing_prevents_using_turbo():
                self.turboAvailable = False
                return True
        return False

    def nothing_prevents_using_turbo(self):
        lookAhead = 5
        if self.track_is_reasonably_straight(lookAhead):
            print("Track is straight, blast away with turbo!")
            return True
        return False

    def track_is_reasonably_straight(self, lookAhead):
        trackIsStraight = 1
        currentPieceIdx = self.currentPosition['piecePosition']['pieceIndex']
        for piece in xrange(lookAhead):
            trackIsStraight &= self.track_piece_is_straight(currentPieceIdx+piece)
        return trackIsStraight

    def track_piece_is_straight(self, index):
        piece =  self.raceTrack.track_piece_at_index(index)
        if piece['angle'] <= 0:
            return 1
        else:
            return 0
