﻿class Action(object):
    def __init__(self, actionType, actionData):
        self.actionType = actionType
        self.actionData = actionData

    def getType(self):
        return self.actionType

    def getData(self):
        return self.actionData


class Throttle(Action):
    def __init__(self, amount):
        super(Throttle, self).__init__('throttle', amount)


class Turbo(Action):
    def __init__(self):
        super(Turbo, self).__init__('turbo', 'ENGAGE_TURBO')


class Ping(Action):
    def __init__(self):
        super(Ping, self).__init__('ping', {})


class Join(Action):
    def __init__(self, name, key):
        super(Join, self).__init__('join', {'name': name,
                                            'key':  key})


class JoinRace(Action):
    def __init__(self, name, key, track_name, password, car_count):
        super(JoinRace, self).__init__('joinRace',
                                       {'botId': {'name': name,
                                                  'key':  key},
                                        'trackName': track_name,
                                        'password': password,
                                        'carCount': car_count})


class CreateRace(Action):
    def __init__(self, name, key, track_name, password, car_count):
        super(CreateRace, self).__init__('createRace',
                                         {'botId': {'name': name,
                                                    'key':  key},
                                          'trackName': track_name,
                                          'password': password,
                                          'carCount': car_count})


class SwitchLane(Action):
    def __init__(self, direction):
        super(SwitchLane, self).__init__('switchLane', direction)
