﻿class Location(object):

    def __init__(self, piecePosition, raceTrack):
        idx = piecePosition['pieceIndex']
        piece = raceTrack.track_piece_at_index(idx)
        self.initialize(idx,
                        piecePosition['inPieceDistance'],
                        piece['radius'],
                        piece['length'],
                        piecePosition['lane']['startLaneIndex'],
                        piecePosition['lane']['endLaneIndex'],
                        piece['angle'],
                        raceTrack)

    def initialize(self, idx, pos, radius, length, lane1, lane2, angle, raceTrack):
        self.idx    = idx
        self.pos    = pos

        self.radius = radius
        self.length = length

        self.lane   = lane1
        switchingLanes = lane1 != lane2
        inCurve = self.radius > 0
        if switchingLanes:
            # estimated from logs
            if inCurve:
                self.length *= 1.03202 # from 22.5 degree curve
            else:
                self.length *= 1.020602722844

        # For curves, adjust length based on current lane.
        # positive angle --> turn right/clockwise
        # positive lane distance --> right
        # When switching lanes, just use the original centerline length
        # FIXME: does not take into account switches not across centerline e.g. when more than 2 lanes
        self.angle = angle
        if not switchingLanes and inCurve:
            sign = -1.0 if self.angle > 0 else 1.0
            centerRadius = self.radius
            self.radius = centerRadius + sign*raceTrack.lane_distances()[lane1]
            self.length = self.length * self.radius/centerRadius
        

    def distance(self, otherLocation):
        # Assumption: otherLocation is always either same piece or previous track piece.
        if otherLocation.idx != self.idx:
            return abs(self.pos + abs(otherLocation.length - otherLocation.pos))
        else:
            return abs(self.pos - otherLocation.pos)

    def advance(self, speed, raceTrack):
        self.pos += speed
        overrun = self.pos - self.length
        if (overrun >= 0.0):
            # Assumption: can never overrun a whole piece
            nextIdx = self.idx + 1
            nextPiece = raceTrack.track_piece_at_index(nextIdx)
            self.initialize(nextIdx, overrun, nextPiece['radius'],
                            nextPiece['length'], self.lane, self.lane,
                            nextPiece['angle'], raceTrack)

class DummyLocation(object):

    def __init__(self):
        self.idx    = -1
        self.pos    = 0.0
        self.radius = -1
        self.length = 0.0
        self.angle  = 0.0

    def distance(self, otherLocation):
        return 0.0

    def advance(self, speed, raceTrack):
        return self
