import socket
import sys
from BotInterface import BotInterface
from NoobBot import NoobBot

botType = NoobBot

if __name__ == "__main__":
    if len(sys.argv) != 5 and len(sys.argv) != 9:
        print("Usage: ./run host port botname botkey [join|create trackName "
              "password carCount]")
        sys.exit(0)

    host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    #s.settimeout(1) # Disable this for tournament.
    bot = BotInterface(s, name, key, botType)
    if len(sys.argv) == 9:
        mode, track_name, password, car_count = sys.argv[5:9]
        bot.run(mode=mode, track_name=track_name, password=password,
                car_count=car_count)
    else:
        bot.run()
    s.shutdown(socket.SHUT_RDWR)
    s.close()
