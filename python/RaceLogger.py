import time, datetime, os, binascii, csv
import collections

CSV_DELIMITER = '\t'
RING_BUFFER_LENGTH = 20

def _generateName():
    return str(int(time.time())) + '_' + binascii.hexlify(os.urandom(2))


class RaceLogger(object):
    def __init__(self, name='LoggerName', *header):
        self.name       = name
        self.ringBuffer = collections.deque(maxlen=RING_BUFFER_LENGTH)
        logFileName     = name + '_' + _generateName() + '.tsv'
        self.logFile    = open(logFileName, 'wb')
        self.logWriter  = csv.writer(self.logFile, delimiter=CSV_DELIMITER)
        self.logEvent(*header)

    def __del__(self):
        self.logFile.close()

    def logEvent(self, *items):
        self.ringBuffer.append(items)
        self.logWriter.writerow(items)

    def printLatest(self):
        for x in self.ringBuffer:
            print self.name, x


class FilteredRaceLogger(object):
    def __init__(self, myName, logFileNamePrefix):
        self.name               = myName
        self.filteredLogFile    = open(logFileNamePrefix + '_filtered.tsv', 'wb')
        self.logWriter          = csv.writer(self.filteredLogFile, delimiter='\t')
        self.logWriter.writerow(('direction', 'tick', 'type', 'data1', 'data2'))

    def __del__(self):
        self.filteredLogFile.close()

    def logRx(self, gameTick, msgType, msgData):
        try:
            if 'carPositions' == msgType:
                for car in msgData:
                    if self.name == car['id']['name']:
                        self.logPosition(gameTick, car)
                        return
            if 'crash' == msgType and self.name == msgData['name']:
                self.logCrash(gameTick)
        except Exception as e:
            raise
            print 'logger failure', type(e), e

    def logTx(self, msgType, msgData):
        if 'throttle' == msgType:
            self.logWriter.writerow(('tx', -1, msgType, msgData))

    def logPosition(self, gameTick, carData):
        position = carData['piecePosition']['inPieceDistance']
        angle    = carData['angle']
        self.logWriter.writerow(('rx', gameTick, 'position', position, angle))

    def logCrash(self, gameTick):
        self.logWriter.writerow(('rx', gameTick, 'crash'))
