﻿import math

import LineEq

def calcOmega(radius, angle, speed):
    if radius > 0:
        sign = 1.0 if angle > 0 else -1.0
        return LineEq.safeDiv(sign*speed*180.0, math.pi*radius)
    else:
        return 0.0
