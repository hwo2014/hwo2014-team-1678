import unittest
from RaceTrack import RaceTrack
from RaceTrackGraph import RaceTrackGraph
from testMessages import *
from pygraph.classes.digraph import digraph
import dijkstra

class RaceTrackGraphTest(unittest.TestCase):

    def test_trackWithOnePiece(self):
        raceTrack = RaceTrack(gameInitOnePieceTrack)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_edge(('p0l0','p0l0'),13.75)
        refGraph.add_edge(('p0l1','p0l1'),11.25)
        self.assertEqual(refGraph, raceTrackGraph.trackGraph)

    def test_trackWithTwoPieces(self):
        raceTrack = RaceTrack(gameInitTwoPieceTrack)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_node('p1l0')
        refGraph.add_node('p1l1')
        refGraph.add_edge(('p0l0','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l1'),100.0)
        refGraph.add_edge(('p1l0','p0l0'),13.75)
        refGraph.add_edge(('p1l1','p0l1'),11.25)
        #Lane switch edges
        refGraph.add_edge(('p0l0','p1l1'),100.0)
        refGraph.add_edge(('p0l1','p1l0'),100.0)

        self.assertEqual(refGraph, raceTrackGraph.trackGraph)

    def test_trackWithTwoPiecesCornerSwitch(self):
        raceTrack = RaceTrack(gameInitTwoPieceTrackCornerSwitch)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_node('p1l0')
        refGraph.add_node('p1l1')
        refGraph.add_edge(('p0l0','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l1'),100.0)
        refGraph.add_edge(('p1l0','p0l0'),13.75)
        refGraph.add_edge(('p1l1','p0l1'),11.25)
        #Lane switch edges
        refGraph.add_edge(('p1l0','p0l1'),13.75)
        refGraph.add_edge(('p1l1','p0l0'),13.75)

        self.assertEqual(refGraph, raceTrackGraph.trackGraph)

    def test_trackWithTwoPiecesThreeLanesWithoutSwitches(self):
        raceTrack = RaceTrack(gameInitTwoPieceTrackThreeLanesWithoutSwitches)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_node('p0l2')
        refGraph.add_node('p1l0')
        refGraph.add_node('p1l1')
        refGraph.add_node('p1l2')

        refGraph.add_edge(('p0l0','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l1'),100.0)
        refGraph.add_edge(('p0l2','p1l2'),100.0)
        refGraph.add_edge(('p1l0','p0l0'),13.75)
        refGraph.add_edge(('p1l1','p0l1'),11.25)
        refGraph.add_edge(('p1l2','p0l2'),10.00)
        self.assertEqual(refGraph, raceTrackGraph.trackGraph)

    def test_trackWithTwoPiecesThreeLanesWithSwitch(self):
        raceTrack = RaceTrack(gameInitTwoPieceTrackThreeLanesWithSwitch)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_node('p0l2')
        refGraph.add_node('p1l0')
        refGraph.add_node('p1l1')
        refGraph.add_node('p1l2')

        refGraph.add_edge(('p0l0','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l1'),100.0)
        refGraph.add_edge(('p0l2','p1l2'),100.0)
        refGraph.add_edge(('p1l0','p0l0'),13.75)
        refGraph.add_edge(('p1l1','p0l1'),11.25)
        refGraph.add_edge(('p1l2','p0l2'),10.00)
        #Switches
        refGraph.add_edge(('p0l0','p1l1'),100.0)
        refGraph.add_edge(('p0l1','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l2'),100.0)
        refGraph.add_edge(('p0l2','p1l1'),100.0)
        self.assertEqual(refGraph, raceTrackGraph.trackGraph)

    def test_trackWithTwoPieceShortestPath(self):
        raceTrack = RaceTrack(gameInitTwoPieceTrack)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        refGraph = digraph()
        refGraph.add_node('p0l0')
        refGraph.add_node('p0l1')
        refGraph.add_node('p1l0')
        refGraph.add_node('p1l1')
        refGraph.add_edge(('p0l0','p1l0'),100.0)
        refGraph.add_edge(('p0l1','p1l1'),100.0)
        refGraph.add_edge(('p1l0','p0l0'),13.75)
        refGraph.add_edge(('p1l1','p0l1'),11.25)
        #Lane switch edges
        refGraph.add_edge(('p0l0','p1l1'),100.0)
        refGraph.add_edge(('p0l1','p1l0'),100.0)

        [refDist, refPrev] = dijkstra.shortest_path(refGraph, 'p0l0', 'p1l1')
        [dist, prev] = dijkstra.shortest_path(raceTrackGraph.trackGraph, 'p0l0', 'p1l1')
        self.assertEqual(refDist, dist)
        self.assertEqual(refPrev, prev)

    def test_fullTrackKeimolaShort(self):
        raceTrack = RaceTrack(gameInitKeimolaShort)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        endNode = 'p10l0'
        [dist, prev] = dijkstra.shortest_path(raceTrackGraph.trackGraph, 'p0l0', endNode)

        dictb = dijkstra.reverse_prev_list(prev)
        print("Shortest path: ")
        node = dictb['p0l0']
        while node != endNode:
            print(node)
            node = dictb[node]

        assert(raceTrackGraph)

    def test_getTrackGraphStartingAt(self):
        raceTrack = RaceTrack(gameInitKeimolaShort)
        raceTrackGraph = RaceTrackGraph(raceTrack)
        pieceIdx = 0
        laneIdx  = 0
        lookAhead = 5
        raceTrackGraph.get_track_graph_recursively_from(pieceIdx, laneIdx, lookAhead)



if __name__ == '__main__':
    unittest.main()