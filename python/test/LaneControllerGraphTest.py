import unittest
import re
from LaneControllerGraph import LaneControllerGraph
from testMessages import *
from RaceTrack import RaceTrack
from RaceTrackGraph import RaceTrackGraph
import dijkstra

class LaneControllerGraphTest(unittest.TestCase):

    def setUp(self):
        self.raceTrack = RaceTrack(gameInitKeimolaShort)
        self.laneController = LaneControllerGraph(self.raceTrack)
        self.trackGraph = RaceTrackGraph(self.raceTrack)

    def test_re(self):
        startNode = 'p0l0'
        lookAhead = 5
        endNodes = self.laneController.derive_end_nodes(startNode, lookAhead, self.raceTrack.number_of_lanes())
        self.assertEqual(['p5l0','p5l1'],endNodes)

    def test_callingShortestPathWillNotFail(self):
        startNode = 'p0l0'
        lookAhead = 5
        endNodes = self.laneController.derive_end_nodes(startNode, lookAhead, self.raceTrack.number_of_lanes())
        path = self.laneController.shortest_path(self.trackGraph.trackGraph, startNode, lookAhead)
        node = path[startNode]
        while node not in endNodes:
            node = path[node]
        self.assertEqual('p5l1', node)

    def test_wrapShortestPathAroundTrackFinishLine(self):
        startNode = 'p10l0'
        lookAhead = 5
        endNodes = self.laneController.derive_end_nodes(startNode, lookAhead, self.raceTrack.number_of_lanes())
        path = self.laneController.shortest_path(self.trackGraph.trackGraph, startNode, lookAhead)
        node = path[startNode]
        while node not in endNodes:
            node = path[node]
        self.assertEqual('p3l0', node)


if __name__ == '__main__':
    unittest.main()