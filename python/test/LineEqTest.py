import unittest

import LineEq

class LineEqTest(unittest.TestCase):

    def setUp(self):
        pass

    def testTrivial(self):
        x1, y1 = 1, 1
        x2, y2 = 0, 0
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 1)
        self.assertAlmostEqual(x, 0)
        self.assertAlmostEqual(y, 0)

    def testSlope(self):
        x1, y1 = 0, 0
        x2, y2 = 1, 2
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 2)
        self.assertAlmostEqual(x, 0)
        self.assertAlmostEqual(y, 0)

    def testBothPos(self):
        x1, y1 = 2, 2
        x2, y2 = 3, 3
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 1)
        self.assertAlmostEqual(x, 0)
        self.assertAlmostEqual(y, 0)

    def testBothNeg(self):
        x1, y1 = -2, -2
        x2, y2 = -3, -3
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 1)
        self.assertAlmostEqual(x, 0)
        self.assertAlmostEqual(y, 0)

    def testPosNeg(self):
        x1, y1 =  1,  3
        x2, y2 = -3, -1
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 1)
        self.assertAlmostEqual(x, -2)
        self.assertAlmostEqual(y, 2)

    def testPosNegIntercepts(self):
        x1, y1 =  1,  1
        x2, y2 = -1, -3
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 2)
        self.assertAlmostEqual(x, 0.5)
        self.assertAlmostEqual(y, -1)

    def testNegPos(self):
        x1, y1 =  4,  -0.5
        x2, y2 = -3, 3
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, -0.5)
        self.assertAlmostEqual(x, 3)
        self.assertAlmostEqual(y, 1.5)

    def testPosNegIntercepts(self):
        x1, y1 =  1,  1
        x2, y2 = -1, -3
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 2)
        self.assertAlmostEqual(x, 0.5)
        self.assertAlmostEqual(y, -1)

    def testHorizontal(self):
        x1, y1 = 1, 1
        x2, y2 = 2, 1
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertAlmostEqual(m, 0)
        self.assertEqual(str(x), 'nan')
        self.assertEqual(y, 1)

    def testVertical(self):
        x1, y1 = -2, 1
        x2, y2 = -2, -1
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertEqual(str(m), 'nan')
        self.assertAlmostEqual(x, -2)
        self.assertEqual(str(y), 'nan')

    def testDegenerate(self):
        x1, y1 = 0, 0
        x2, y2 = 0, 0
        m, x, y = LineEq.slopeAndIntercepts(x1, y1, x2, y2)
        self.assertEqual(str(m), 'nan')
        self.assertEqual(str(x), 'nan')
        self.assertEqual(str(y), 'nan')

if __name__ == '__main__':
    unittest.main()
