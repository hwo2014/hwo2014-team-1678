import unittest

from testMessages   import gameInitKeimola
from LaneController import LaneController
from RaceTrack      import RaceTrack

carPositionMsgSection0 = \
    {
        'piecePosition':
            {
                'pieceIndex': 2,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

carPositionMsgSection1 = \
    {
        'piecePosition':
            {
                'pieceIndex': 5,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

class LaneControllerTest(unittest.TestCase):

    def setUp(self):
        self.raceTrack      = RaceTrack(gameInitKeimola)
        self.laneController = LaneController(self.raceTrack)

    def test_getCurrentSectionIndexIs0(self):
        self.assertEqual(0, self.laneController.get_current_section_index(carPositionMsgSection0))

    def test_getCurrentSectionIndexIs1(self):
        self.assertEqual(1, self.laneController.get_current_section_index(carPositionMsgSection1))

    def test_laneSwitchingIsRequired(self):
        switchLane, switchTo = self.laneController.switch_lane_now(100, carPositionMsgSection0)
        self.assertEqual(1, switchLane)

    def test_laneSwitchingNotRequired(self):
        switchLane, switchTo = self.laneController.switch_lane_now(100, carPositionMsgSection1)
        self.assertEqual(0, switchLane)


if __name__ == '__main__':
    unittest.main()