import unittest
from TurboController import TurboController
from LaneController import LaneController
from RaceTrack import RaceTrack
from testMessages import *

class LaneSwitcherSimpleTest(unittest.TestCase):

    def setUp(self):
        self.raceTrack = RaceTrack(gameInitKeimola)
        self.laneController = LaneController(self.raceTrack)
        self.turboController = TurboController(self.laneController, self.raceTrack)

    def test_trackPieceIsStraight(self):
        self.assertEqual(1, self.turboController.track_piece_is_straight(0))

    def test_trackPieceIsNotStraight(self):
        self.assertEqual(0, self.turboController.track_piece_is_straight(4))

    def test_trackIsReasonablyStraightForTurbo(self):
        self.turboController.use_turbo_now(carPositionMsgAtIndex0)
        lookAhead = 2
        self.assertEqual(True, self.turboController.track_is_reasonably_straight(lookAhead))

    def test_trackIsNOTReasonablyStraightForTurbo(self):
        self.turboController.use_turbo_now(carPositionMsgAtIndex4)
        lookAhead = 2
        self.assertEqual(False, self.turboController.track_is_reasonably_straight(lookAhead))


if __name__ == '__main__':
    unittest.main()