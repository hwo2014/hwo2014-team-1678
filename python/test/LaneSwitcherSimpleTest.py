import unittest
from LaneSwitcherSimple import LaneSwitcherSimple
from RaceTrack          import RaceTrack

onePieceTrackWithTwoLanesRightBend = {'race':
                                          {'track':
                                               {'pieces': [{'angle': 45.0, 'radius': 100}],
                                                'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
                                               }
                                          }
}
onePieceTrackWithTwoLanesLeftBend = {'race':
                                          {'track':
                                               {'pieces': [{'angle': -45.0, 'radius': 100}],
                                                'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
                                               }
                                          }
}
onePieceTrackWithThreeLanesRightBend = {'race':
                                          {'track':
                                               {'pieces': [{'angle': 35.0, 'radius': 100}],
                                                'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}, {'index': 2, 'distanceFromCenter': 20}],
                                               }
                                          }
}
threePieceTrackWithTwoLanesRightBendAndSwitch = {'race':
                                                   {'track':
                                                        {'pieces': [{'length': 100, 'switch': 0},
                                                                    {'length': 100, 'switch': 1},
                                                                    {'angle': 35.0, 'radius': 100}],
                                                         'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
                                                         }
                                                   }
}
raceInitKeimola = {'race':
                       {'track':
                            {'pieces': [{'length': 100.0},
                                        {'length': 100.0},
                                        {'length': 100.0},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 22.5, 'switch': True, 'radius': 200},
                                        {'length': 100.0},
                                        {'length': 100.0},
                                        {'angle': -22.5, 'radius': 200},
                                        {'length': 100.0},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 22.5, 'radius': 200},
                                        {'angle': -22.5, 'radius': 200},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'length': 62.0},
                                        {'angle': -45.0, 'switch': True, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'length': 100.0, 'switch': True},
                                        {'length': 100.0}, {'length': 100.0},
                                        {'length': 100.0}, {'length': 90.0}],
                             'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}]
                            }
                       }
}

class LaneSwitcherSimpleTest(unittest.TestCase):

    def test_laneOffsets(self):
        raceTrack = RaceTrack(onePieceTrackWithTwoLanesRightBend)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([-10, 10], self.laneSwitcher.get_lane_offsets())

    def test_onePieceTrackWithTwoLanesRightBend(self):
        raceTrack = RaceTrack(onePieceTrackWithTwoLanesRightBend)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([1], self.laneSwitcher.get_best_lane_indexes())

    def test_onePieceTrackWithTwoLanesLeftBend(self):
        raceTrack = RaceTrack(onePieceTrackWithTwoLanesLeftBend)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([0], self.laneSwitcher.get_best_lane_indexes())

    def test_onePieceTrackWithThreeLanesRightBend(self):
        raceTrack = RaceTrack(onePieceTrackWithThreeLanesRightBend)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([2], self.laneSwitcher.get_best_lane_indexes())

    def test_threePieceTrackWithTwoLanesRightBend(self):
        raceTrack = RaceTrack(threePieceTrackWithTwoLanesRightBendAndSwitch)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([0, 1], self.laneSwitcher.get_best_lane_indexes())

    def test_keimola(self):
        raceTrack = RaceTrack(raceInitKeimola)
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.assertEqual([0, 1, 0, 0, 1, 1, 1], self.laneSwitcher.get_best_lane_indexes())


if __name__ == '__main__':
    unittest.main()
