""" Generic test messages. """

gameInitKeimola = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0},
                          {'length': 100.0},
                          {'length': 100.0},
                          {'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 22.5, 'switch': True, 'radius': 200},
                          {'length': 100.0},
                          {'length': 100.0},
                          {'angle': -22.5, 'radius': 200},
                          {'length': 100.0},
                          {'length': 100.0, 'switch': True},
                          {'angle': -45.0, 'radius': 100},
                          {'angle': -45.0, 'radius': 100},
                          {'angle': -45.0, 'radius': 100},
                          {'angle': -45.0, 'radius': 100},
                          {'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 22.5, 'radius': 200},
                          {'angle': -22.5, 'radius': 200},
                          {'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'length': 62.0},
                          {'angle': -45.0, 'switch': True, 'radius': 100},
                          {'angle': -45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'length': 100.0, 'switch': True},
                          {'length': 100.0}, {'length': 100.0},
                          {'length': 100.0}, {'length': 90.0}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10},
                         {'index': 1, 'distanceFromCenter': 10}]
              }
         }
    }

gameInitKeimolaShort = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0},
                          {'length': 100.0},
                          {'length': 100.0},
                          {'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 45.0, 'radius': 100},
                          {'angle': 22.5, 'switch': True, 'radius': 200},
                          {'length': 100.0},
                          {'length': 100.0}, {'length': 90.0}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10},
                         {'index': 1, 'distanceFromCenter': 10}]
              }
         }
    }

gameInitOnePieceTrack = \
    {'race':
         {'track':
              {'pieces': [{'angle': 45.0, 'radius': 100}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
               }
         }
    }
gameInitTwoPieceTrack = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100.0}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
               }
         }
    }

gameInitTwoPieceTrackCornerSwitch = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0, 'switch': False},
                          {'angle': 45.0, 'radius': 100.0, 'switch': True}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
               }
         }
    }

gameInitTwoPieceTrackThreeLanesWithoutSwitches = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0, 'switch': False},
                          {'angle': 45.0, 'radius': 100.0}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}, {'index': 1, 'distanceFromCenter': 20}],
               }
         }
    }


gameInitTwoPieceTrackThreeLanesWithSwitch = \
    {'race':
         {'track':
              {'pieces': [{'length': 100.0, 'switch': True},
                          {'angle': 45.0, 'radius': 100.0}],
               'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}, {'index': 1, 'distanceFromCenter': 20}],
               }
         }
    }


carPositionMsgAtIndex0 = \
    {
        'piecePosition':
            {
                'pieceIndex': 0,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

carPositionMsgAtIndex3 = \
    {
        'piecePosition':
            {
                'pieceIndex': 3 ,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

carPositionMsgAtIndex4 = \
    {
        'piecePosition':
            {
                'pieceIndex': 4 ,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

carPositionMsgAtIndex10 = \
    {
        'piecePosition':
            {
                'pieceIndex': 10 ,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }

carPositionMsgAtIndex12 = \
    {
        'piecePosition':
            {
                'pieceIndex': 12 ,
                'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                'lap': 0,
                'inPieceDistance': 10.589449126666494
            },
        'angle': 26.422644891192146,
        'id':
            {'color': 'red',
             'name': 'Zero Reaction'
            }
    }
