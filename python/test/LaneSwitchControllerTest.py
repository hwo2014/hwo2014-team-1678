import unittest
from LaneSwitcherController import LaneSwitcherController
from RaceTrack import RaceTrack
from testMessages import *

class LaneSwitcherControllerTest(unittest.TestCase):

    def setUp(self):
        self.raceTrack = RaceTrack(gameInitKeimolaShort)
        self.laneSwitcher = LaneSwitcherController(self.raceTrack)

    def test_callingSwitchLaneNowWillNotFail(self):
        gameTick = 11
        carPositionMsg = carPositionMsgAtIndex0
        self.laneSwitcher.switch_lane_now(gameTick, carPositionMsg)
        pass

    def test_doNotSwitchLaneAtStart(self):
        gameTick = 11
        carPositionMsg = carPositionMsgAtIndex0
        switchLane, switchTo = self.laneSwitcher.switch_lane_now(gameTick, carPositionMsg)
        self.assertEqual(0, switchLane)

    def test_laneSwitchingNotDoneDueToGameTickTooSmall(self):
        gameTick = 11
        carPositionMsg = carPositionMsgAtIndex3
        switchLane, switchTo = self.laneSwitcher.switch_lane_now(gameTick, carPositionMsg)
        self.assertEqual(0, switchLane)

    def test_laneSwitchingInitiated(self):
        gameTick = 16
        carPositionMsg = carPositionMsgAtIndex3
        switchLane, switchTo = self.laneSwitcher.switch_lane_now(gameTick, carPositionMsg)
        self.assertEqual(1, switchLane)

    def test_laneSwitchingNotInitiated(self):
        self.raceTrack = RaceTrack(gameInitKeimola)
        self.laneSwitcher = LaneSwitcherController(self.raceTrack)
        gameTick = 100
        carPositionMsg = carPositionMsgAtIndex12
        switchLane, switchTo = self.laneSwitcher.switch_lane_now(gameTick, carPositionMsg)
        self.assertEqual(0, switchLane)

if __name__ == '__main__':
    unittest.main()