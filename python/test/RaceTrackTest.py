__author__ = 'jaaho'

import unittest
from RaceTrack import RaceTrack

testMessage = {
    "race": {
        "track": {
            "id": "indianapolis",
            "name": "Indianapolis",
            "pieces": [
                {
                    "length": 100.0
                },
                {
                    "length": 100.0,
                    "switch": True
                },
                {
                    "radius": 200,
                    "angle": 22.5
                }
            ],
            "lanes": [
                {
                    "distanceFromCenter": -20,
                    "index": 0
                },
                {
                    "distanceFromCenter": 0,
                    "index": 1
                },
                {
                    "distanceFromCenter": 20,
                    "index": 2
                }
            ],
            "startingPoint": {
                "position": {
                    "x": -340.0,
                    "y": -96.0
                },
                "angle": 90.0
            }
        }
    }
}

class RaceTrackTest(unittest.TestCase):

    def setUp(self):
        self.newTrack = RaceTrack(testMessage)

    def test_parseGameInit(self):
        self.assertEqual(3, len(self.newTrack.track_pieces))

    def test_getTrackLenInPieces(self):
        self.assertEqual(3, len(self.newTrack))

    def test_getFirstTrackPiece(self):
        self.assertEqual({'length' : 100.0,
                          'switch' : False,
                          'radius' : -1.0,
                          'angle'  : 0},
                         self.newTrack.track_piece_at_index(0))

    def test_getLastPlusOneTrackPiece(self):
        self.assertEqual({'length' : 100.0,
                          'switch' : False,
                          'radius' : -1.0,
                          'angle'  : 0},
                         self.newTrack.track_piece_at_index(3))

    def test_getNumOfLanes(self):
        self.assertEqual(3, self.newTrack.number_of_lanes())

    def test_numberOfTrackSections(self):
        self.assertEqual(1, self.newTrack.number_of_track_sections())

if __name__ == '__main__':
    unittest.main()
