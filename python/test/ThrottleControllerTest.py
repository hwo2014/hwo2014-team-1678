__author__ = 'jaaho'

from ThrottleController import ThrottleController

import unittest

from testMessages       import gameInitKeimola
from LaneController     import LaneController
from RaceTrack          import RaceTrack
from TrackPhysics       import TrackPhysics

raceInitKeimola = {'race':
                       {'track':
                            {'pieces': [{'length': 100.0},
                                        {'length': 100.0},
                                        {'length': 100.0},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 22.5, 'switch': True, 'radius': 200},
                                        {'length': 100.0},
                                        {'length': 100.0},
                                        {'angle': -22.5, 'radius': 200},
                                        {'length': 100.0},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 22.5, 'radius': 200},
                                        {'angle': -22.5, 'radius': 200},
                                        {'length': 100.0, 'switch': True},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'length': 62.0},
                                        {'angle': -45.0, 'switch': True, 'radius': 100},
                                        {'angle': -45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'angle': 45.0, 'radius': 100},
                                        {'length': 100.0, 'switch': True},
                                        {'length': 100.0}, {'length': 100.0},
                                        {'length': 100.0}, {'length': 90.0}],
                             'lanes': [{'index': 0, 'distanceFromCenter': -10}, {'index': 1, 'distanceFromCenter': 10}],
                             'id': 'keimola',
                             'startingPoint': {'position': {'y': -44.0, 'x': -300.0}, 'angle': 90.0},
                             'name': 'Keimola'},
                        'cars': [{'id': {'color': 'red', 'name': 'Zero Reaction'}, 'dimensions': {'width': 20.0, 'length': 40.0, 'guideFlagPosition': 10.0}}],
                        'raceSession': {'laps': 3, 'maxLapTimeMs': 60000, 'quickRace': True}
                       }
}

class ThrottleControllerTest(unittest.TestCase):

    def setUp(self):
        raceTrack       = RaceTrack(gameInitKeimola)
        laneController  = LaneController(raceTrack)
        trackPhysics    = TrackPhysics()
        self.throttleController = ThrottleController(laneController, raceTrack, trackPhysics)

    def test_throttleOne(self):
        myPosData = 1 # FIXME
        self.assertEqual(1.0, self.throttleController.get_throttle(1, myPosData))

if __name__ == '__main__':
    unittest.main()
