import json
import traceback
import time

import Actions
from RaceLogger import RaceLogger

DEFAULT_ACTION = Actions.Throttle(0.5)


class BotInterface(object):

    def __init__(self, socket, name, key, bot_type):
        self.socket = socket
        self.sf = socket.makefile()
        self.name = name
        self.key = key
        self.logger = RaceLogger('race', 'gameTick', 'time', 'clock', 'rxType',
                                 'rxData', 'txType', 'txData')
        self.bot = bot_type(name)
        self.msg_map = {
            'join':           self.bot.on_join,
            'gameInit':       self.bot.on_game_init,
            'createRace':     self.bot.on_create_race,
            'joinRace':       self.bot.on_join_race,
            'gameStart':      self.bot.on_game_start,
            'carPositions':   self.bot.on_car_positions,
            'turboAvailable': self.bot.on_turbo_available,
            'turboStart':     self.bot.on_turbo_start,
            'turboEnd':       self.bot.on_turbo_end,
            'crash':          self.bot.on_crash,
            'spawn':          self.bot.on_spawn,
            'gameEnd':        self.bot.on_game_end,
            'error':          self.bot.on_error,
            'yourCar':        self.bot.on_your_car,
            'lapFinished':    self.bot.on_lap_finished}

    def run(self, mode='', track_name='', password='',
            car_count='3'):
        try:
            car_count_num = int(car_count)
        except ValueError:
            car_count_num = 3
        if mode.lower() == 'create':
            print 'Creating new race:\n    Track: %s\n    Password: %s\n    '\
                  'Car Count: %s' % (track_name, password, car_count)
            self.respond(Actions.CreateRace(self.name, self.key, track_name,
                                            password, car_count_num))
        elif mode.lower() == 'join':
            print 'Joining private race:\n    Track: %s\n    Password: %s\n' \
                  '    Car Count: %s' % (track_name, password, car_count)
            self.respond(Actions.JoinRace(self.name, self.key, track_name,
                                          password, car_count_num))
        else:
            print 'Joining standard race'
            self.respond(Actions.Join(self.name, self.key))
        self.msg_loop()

    def msg_loop(self):
        line = self.sf.readline()
        while line:
            line = self.process(line)

    def process(self, line):
        start_time, start_clock = time.time(), time.clock()
        msg = json.loads(line)
        msg_type, msg_data = msg['msgType'], msg['data']
        try:
            game_tick = msg['gameTick']
        except KeyError:
            game_tick = -1
        if 0 == 31 & (2 + game_tick):
            print 'tick', game_tick  # DEBUG race progress print
        action = DEFAULT_ACTION
        if msg_type in self.msg_map:
            try:
                action = self.msg_map[msg_type](game_tick, msg_data)
            except Exception as e:
                print 'BotInterface AI exception after', msg_type, msg_data,\
                    type(e), e
                traceback.print_exc(e)
                #traceback.print_stack()
            if action is None:
                print 'BotInterface AI error, no action returned for',\
                    msg_type, msg_data
                action = DEFAULT_ACTION
        else:
            print 'unhandled message:', msg_type  # , msgData
        if action:
            diff_time, diff_clock = (1000000*(time.time() - start_time),
                                     1000000*(time.clock() - start_clock))
            self.logger.logEvent(game_tick, diff_time, diff_clock, msg_type,
                                 msg_data, action.getType(), action.getData())
            self.respond(action)
        return self.sf.readline()

    def respond(self, action):
        encoded = json.dumps({'msgType': action.getType(),
                              'data': action.getData()})
        self.socket.send(encoded + '\n')
