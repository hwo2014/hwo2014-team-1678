import Actions
from RaceTrack          import RaceTrack
from RaceLogger         import RaceLogger
from Location           import Location, DummyLocation
#from LaneController     import LaneController
from ThrottleController import ThrottleController
from TurboController    import TurboController
from CarState           import CarState
from TrackPhysics       import Acceleration, Drift
from LaneSwitcherController import LaneSwitcherController

class NoobBot(object):

    def __init__(self, name):
        self.name = name
        self.logger = RaceLogger(self.__class__.__name__,
                'type', 'tick', 'angle', 'throttle', 'speed', 'omega',
                'drift', 'pA1', 'pS1', 'pA2', 'pS2', 'accel', 'm', 'x',
                'y', *6*('dbg', ))
        self.carState      = CarState()
        self.acceleration  = Acceleration()
        self.drift         = Drift()
        self.color         = ''
        self.trackMeasured = False
        self._crashed = False
        self._turbo_factor = 1
        self.throttleController = ThrottleController(None,
                                                     self.carState,
                                                     self.acceleration,
                                                     self.drift)

    def on_join(self, tick, data):
        data.pop('key')
        self._print(tick, 'joined', data)
        return Actions.Ping()

    def on_game_start(self, tick, data):
        self._print(tick, 'game start', data)
        return Actions.Throttle(self.carState.throttle)

    def on_your_car(self, tick, data):
        self._print(tick, 'your car', data)
        self.color = data['color']
        return Actions.Ping()

    def on_game_init(self, tick, data):
        self._print(tick, 'received track info')
        self.raceTrack          = RaceTrack(data)
        self.laneController     = LaneSwitcherController(self.raceTrack)
        self.turboController    = TurboController(self.laneController,
                                                  self.raceTrack)
        self.throttleController.raceTrack = self.raceTrack
        return Actions.Ping()

    def on_create_race(self, tick, data):
        self._print(tick, 'create race', data)
        return Actions.Ping()

    def on_join_race(self, tick, data):
        self._print(tick, 'join race', data)
        return Actions.Ping()

    def on_lap_finished(self, tick, data):
        self._print(tick, 'lap finished', data)
        return Actions.Ping()

    def on_car_positions(self, tick, data):
        myData = filter(lambda x:self.name == x['id']['name'], data)[0]
        self.carState.update(myData, self.raceTrack)
        self.drift.update(self.carState.omega)
        self.log_position(tick, myData)

        trackMeasurementsValid = tick > 4 # hax
        if not self.trackMeasured and trackMeasurementsValid:
            m = self.carState.m
            x = self.carState.x
            y = self.carState.y
            t = self.carState.throttle
            self.acceleration.update(m, x, y, t)
            self.throttleController.go_nuts()
            self.trackMeasured = True
        '''
        if not self._crashed:
            self.throttleController.simple_throttle(self.carState.angle,
                                                    self._turbo_factor,
                                                    self.carState.speed)
        '''

        switchLane, switchTo = self.laneController.switch_lane_now(tick, myData)
        if (switchLane != 0):
            self._print(tick, 'Sent switchLane {0}'.format(switchTo))
            return Actions.SwitchLane(switchTo)
        elif self.turboController.use_turbo_now(myData):
            return Actions.Turbo()
        else:
            newThrottle = self.throttleController.get_throttle()
            self.carState.setThrottle(newThrottle)
            return Actions.Throttle(newThrottle)

    def on_turbo_available(self, tick, data):
        self.turboController.turbo_available(data)
        return Actions.Ping()

    def on_turbo_start(self, tick, data):
        self._turbo_factor = max(
            1, self.turboController.turboDetails['turboFactor'])
        return Actions.Ping()

    def on_turbo_end(self, tick, data):
        self._turbo_factor = 1
        return Actions.Ping()

    def on_crash(self, tick, data):
        if self.name == data['name']:
            self._crashed = True
            self._print(tick, 'crashed, post-mortem follows (latest last):')
            self.logger.printLatest()
            self.logger.logEvent('crash', tick)
        else:
            self._print('someone else crashed', tick, data)
        return Actions.Ping()

    def on_game_end(self, tick, data):
        self._print(tick, 'game end', data)
        return Actions.Ping()

    def on_error(self, tick, data):
        self._print(tick, 'error', data)
        return Actions.Ping()

    def on_spawn(self, tick, data):
        if self.name == data['name']:
            self._print(tick, 'spawn', data)
            self._crashed = False
        return Actions.Ping()

    def log_position(self, tick, myData):
        position = myData['piecePosition']
        dbg = ( position['pieceIndex'],
                self.carState.locations[-2].idx,
                position['inPieceDistance'],
                self.carState.locations[-1].length,
                self.carState.locations[-1].radius,
                self.carState.locations[-1].angle,
        )
        self.logger.logEvent('pos', tick, self.carState.angle,
                self.carState.throttle, self.carState.speed,
                self.carState.omega, self.drift.getDriftEstimate(self.carState.speed),
                self.throttleController.predictedAngle1,
                self.throttleController.predictedSpeed1,
                self.throttleController.predictedAngle2,
                self.throttleController.predictedSpeed2,
                self.carState.accel, self.carState.m, self.carState.x,
                self.carState.y, *dbg)

    def _print(self, tick, name, data=''):
        print self.__class__.__name__, tick, name, data
