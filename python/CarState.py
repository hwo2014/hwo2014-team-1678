﻿import collections
import math

import LineEq
import Misc

from Location import Location, DummyLocation
from TrackPhysics import DEFAULT_M, DEFAULT_X, DEFAULT_Y

START_THROTTLE = 1.0
NUM_LOCATIONS  = 4

class CarState(object):
    def __init__(self):
        self.locations = collections.deque(NUM_LOCATIONS*[DummyLocation()], maxlen=NUM_LOCATIONS)
        self.angle     = 0.0
        self.speed     = 0.0
        self.accel     = 0.0
        self.throttle  = START_THROTTLE # throttle info not needed here
        self.omega     = 0.0
        # move these to somewhere else
        self.m         = DEFAULT_M
        self.x         = DEFAULT_X
        self.y         = DEFAULT_Y

    def update(self, data, raceTrack):
        self.angle = data['angle']
        self.locations.append(Location(data['piecePosition'], raceTrack))
        x1 = self.locations[-1]
        x2 = self.locations[-2]
        x3 = self.locations[-3]
        x4 = self.locations[-4]
        d1 = x1.distance(x2)
        d2 = x2.distance(x3)
        d3 = x3.distance(x4)

        v1 = d1
        v2 = d2
        a1 = d1 - d2
        a2 = d2 - d3
        self.m, self.x, self.y = LineEq.slopeAndIntercepts(v1, a1, v2, a2)

        self.speed = v1
        self.accel = a1
        self.omega = Misc.calcOmega(self.locations[-1].radius, self.locations[-1].angle, self.speed)


    def setThrottle(self, throttle):
        self.throttle = throttle
        return throttle
