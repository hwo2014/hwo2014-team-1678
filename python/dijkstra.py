def shortest_path(digraph, sourceNode, endNodes):
    """
    Return the shortest path distance between sourceNode and all other nodes
    using Dijkstra's algorithm.  See
    http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm.  
    
    @attention All weights must be nonnegative.

    @type  graph: graph
    @param graph: Graph.

    @type  sourceNode: node
    @param sourceNode: Node from which to start the search.

    @rtype  tuple
    @return A tuple containing two dictionaries, each keyed by
        targetNodes.  The first dictionary provides the shortest distance
        from the sourceNode to the targetNode.  The second dictionary
        provides the previous node in the shortest path traversal.
        Inaccessible targetNodes do not appear in either dictionary.
    """
    # Initialization
    q        = digraph.nodes()
    dist = {}
    for node in q:
        dist[node] = 65535
    dist[sourceNode] = 0
    previous = {}
    # Switch source node to be first element in q
    sourceNodexIndex = q.index(sourceNode)
    temp = q[0]
    q[0] = q[sourceNodexIndex]
    q[sourceNodexIndex] = temp
    # print("Edges {0}".format(digraph.edges()))
    # print("Get edge weight {0}".format(digraph.edge_weight((q[0],q[6]))))
    # print("      q.Nodes {0}".format(q))
    # print("Digraph.Nodes {0}".format(digraph.nodes()))
    # print("Staring from {0}".format(sourceNode))

    # Algorithm loop
    while q:
        # examine_min process performed using O(nodes) pass here.
        # May be improved using another examine_min data structure.
        # See http://www.personal.kent.edu/~rmuhamma/Algorithms/MyAlgorithms/GraphAlgor/dijkstraAlgor.htm
        u = q[0]
        for node in q[1:]:
            if (   (not dist.has_key(u))
                   or (dist.has_key(node) and dist[node] < dist[u]) ):
                u = node
        q.remove(u)
        if u in endNodes:
            break
        #print("Nodes {0}".format(q))
        # print("Nodes {0}".format(q))
        # print("Processing {0}".format(u))

        # Process reachable, remaining nodes from u
        # print("Reachable nodes from {0} are {1}".format(u, digraph.neighbors(u)))
        #for v in digraph.incidents(u):
        for v in digraph.neighbors(u):
            if v in q:
                # print("Edge: {0}-{1}".format(u,v))
                alt = dist[u] + digraph.edge_weight((u, v))
                if (not dist.has_key(v)) or (alt < dist[v]):
                    dist[v] = alt
                    previous[v] = u

    return (dist, previous)

def reverse_prev_list(prevList):
    return dict ((v,k) for k, v in prevList.items())