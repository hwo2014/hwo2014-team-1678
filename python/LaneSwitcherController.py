
from RaceTrackGraph import RaceTrackGraph
from LaneControllerGraph import LaneControllerGraph
from OverTakeController import OverTakeController
from pygraph.classes.digraph import digraph
import re

SHORTEST_PATH_LOOKAHEAD    = 10
SUBGRAPH_DEPTH             = SHORTEST_PATH_LOOKAHEAD+1
NO_LANE_SWITCH_BEFORE_TICK = 15

class LaneSwitcherController(object):

    def __init__(self, raceTrack):
        self.raceTrack = raceTrack
        self.raceTrackGraph      = RaceTrackGraph(raceTrack)
        self.subGraph            = digraph()
        self.overTakeController  = OverTakeController(raceTrack)
        self.laneControllerGraph = LaneControllerGraph(raceTrack)
        self.laneSwitchSent      = False
        self.nextBestLane        = 0

    def switch_lane_now(self, gameTick, carPositionMsg):
        switchLane, switchTo = 0, ''
        if gameTick > NO_LANE_SWITCH_BEFORE_TICK:
            pieceIdx = carPositionMsg['piecePosition']['pieceIndex']
            laneIdx  = carPositionMsg['piecePosition']['lane']['startLaneIndex']
            if self.stay_on_current_lane(carPositionMsg, pieceIdx, laneIdx):
                self.laneSwitchSent = False
            else:
                if not self.is_lane_switching_ongoing(carPositionMsg) and not self.laneSwitchSent:
                    switchLane, switchTo = self.lane_switching_parameters(laneIdx)
                    self.laneSwitchSent = True
        return switchLane, switchTo

    def is_lane_switching_ongoing(self, carPositionMsg):
        currentLane = carPositionMsg['piecePosition']['lane']['startLaneIndex']
        nextLane    = carPositionMsg['piecePosition']['lane']['endLaneIndex']
        return currentLane != nextLane

    def ensuing_track_graph(self, carPositionMsg, pieceIdx, laneIdx):
        lookAhead = SUBGRAPH_DEPTH
        return self.raceTrackGraph.get_track_graph_recursively_from(pieceIdx, laneIdx, lookAhead)

    def stay_on_current_lane(self, carPositionMsg, pieceIdx, laneIdx):
        trackGraph = self.ensuing_track_graph(carPositionMsg, pieceIdx, laneIdx)
        startNode = 'p'+`pieceIdx`+'l'+`laneIdx`
        shortestPath = self.laneControllerGraph.shortest_path(trackGraph, startNode, SHORTEST_PATH_LOOKAHEAD)

        endNodes = self.laneControllerGraph.derive_end_nodes(startNode,SHORTEST_PATH_LOOKAHEAD,2)
        #node = shortestPath[startNode]
        #while node not in endNodes:
        #    print node
        #    node = shortestPath[node]

        nextNode = shortestPath[startNode]
        optimalLane = int(re.match(r"p([0-9]+)+l([0-9]+)", shortestPath[nextNode]).group(2))
        currentLane = carPositionMsg['piecePosition']['lane']['startLaneIndex']
        self.nextBestLane = optimalLane
        #print("StartLane {3}, Next best lane: {0}, current p{1}l{2}".format(self.nextBestLane, pieceIdx, laneIdx, startNode))
        return optimalLane == currentLane

    def lane_switching_parameters(self, currentLane):
        switchLane = 0
        optimalLane = self.nextBestLane
        if optimalLane != currentLane:
            switchLane = optimalLane - currentLane
            self.laneSwitchSent = True
        else:
            self.laneSwitchSent = False
        return switchLane, self.convert_lane_delta_to_word(switchLane)

    def convert_lane_delta_to_word(self, deltaDesiredToCurrent):
        if deltaDesiredToCurrent < 0:
            return 'Left'
        elif deltaDesiredToCurrent > 0:
            return 'Right'
        else:
            return ''




