
from pygraph.classes.digraph import digraph # Directed graph

class RaceTrackGraph(object):

    def __init__(self, raceTrack):
        self.trackGraph = digraph() # Create empty directed graph
        self.construct_track_graph(raceTrack)
        self.subGraph = digraph()
        #print("RaceTrack: {0}".format(raceTrack.all_track_pieces()))
        # print("TrackGraphNodes: {0}".format(self.trackGraph.nodes()))

    def construct_track_graph(self, raceTrack):
        numOfLanes = raceTrack.number_of_lanes()
        trackPieces = raceTrack.all_track_pieces()
        # Add first nodes to graph
        previousPiece, switchWeights = self._add_first_piece(raceTrack, numOfLanes)
        for pieceIdx, piece in enumerate(trackPieces):
            if pieceIdx == 0:
                continue
            laneLengths = self.track_piece_lane_lengths(previousPiece, raceTrack.track_lanes)
            previousPieceHadSwitch = self._previous_piece_had_switch(previousPiece)
            for laneIdx, laneLength in enumerate(laneLengths):
                self.trackGraph.add_node(self.format_node(pieceIdx, laneIdx)) # Lane node
                self.trackGraph.add_edge(self.format_edge(pieceIdx-1, pieceIdx, laneIdx, laneIdx), laneLength)
                if previousPieceHadSwitch: # Add switch edges
                    switchWeights = self._get_switch_weights(previousPiece, laneLengths)
                    self._add_switch_edge(pieceIdx-1, pieceIdx, laneIdx, numOfLanes, switchWeights)
            previousPiece = piece

        # Connect first and last pieces
        switchWeights = self._get_switch_weights(previousPiece, self.track_piece_lane_lengths(previousPiece, raceTrack.track_lanes))
        self._connect_first_and_last_pieces(raceTrack, previousPiece, switchWeights)

    def _add_first_piece(self, raceTrack, numOfLanes):
        pieceIdx = 0
        piece = raceTrack.track_piece_at_index(pieceIdx)
        for laneIdx in xrange(numOfLanes):
            self.trackGraph.add_node(self.format_node(pieceIdx, laneIdx)) # Lane node
        laneLengths   = self.track_piece_lane_lengths(piece, raceTrack.track_lanes)
        switchWeights = self._get_switch_weights(piece, laneLengths)
        previousPiece = piece
        return previousPiece, switchWeights

    def _add_switch_edge(self, pieceIdxFrom, pieceIdxTo, laneIdx, numOfLanes, switchWeights):
        switchWeight = switchWeights[laneIdx]
        if laneIdx == 0:
            self.trackGraph.add_edge(self.format_edge(pieceIdxFrom, pieceIdxTo, laneIdx+1, laneIdx), switchWeight)
        elif laneIdx == numOfLanes-1:
            self.trackGraph.add_edge(self.format_edge(pieceIdxFrom, pieceIdxTo, laneIdx-1, laneIdx), switchWeight)
        else:
            self.trackGraph.add_edge(self.format_edge(pieceIdxFrom, pieceIdxTo, laneIdx+1, laneIdx), switchWeight)
            self.trackGraph.add_edge(self.format_edge(pieceIdxFrom, pieceIdxTo, laneIdx-1, laneIdx), switchWeight)

    def _connect_first_and_last_pieces(self, raceTrack, previousPiece, switchWeights):
        pieceIdx = 0
        lastPieceIdx = raceTrack.number_of_track_pieces-1
        piece = raceTrack.track_piece_at_index(lastPieceIdx)
        laneLengths = self.track_piece_lane_lengths(piece, raceTrack.track_lanes)
        numOfLanes = len(laneLengths)
        previousPieceHadSwitch = self._previous_piece_had_switch(previousPiece)
        for laneIdx, laneLength in enumerate(laneLengths):
            self.trackGraph.add_edge(self.format_edge(lastPieceIdx, pieceIdx, laneIdx, laneIdx), laneLength)
            if previousPieceHadSwitch: # Add switch edges
                self._add_switch_edge(lastPieceIdx, pieceIdx, laneIdx, numOfLanes, switchWeights)

    def _previous_piece_had_switch(self, previousPiece):
        if previousPiece.has_key('switch'):
            return previousPiece['switch']
        return False

    def _get_switch_weights(self, piece, laneLengths):
        numOfLanes = len(laneLengths)
        laneSwitchLengths = {} #0: (switchLower, switchHigher)}
        if (piece['radius'] <= 0):
            laneSwitchLengths = laneLengths
        else:
            for laneIdx in xrange(numOfLanes):
                laneSwitchLengths[laneIdx] = max(laneLengths[max(laneIdx-1, 0):min(laneIdx+1, numOfLanes)])

        return laneSwitchLengths

    def track_piece_lane_lengths(self, trackPiece, trackLanes):
        laneLengths = []
        for lane in trackLanes:
            if trackPiece['radius'] > 0:
                laneLengths.append(abs(trackPiece['angle']) / 360 * self.get_lane_radius(trackPiece, lane))
            else:
                laneLengths.append(trackPiece['length'])
        return laneLengths

    def format_node(self, pieceIdx, laneIdx):
        return 'p'+`pieceIdx`+'l'+`laneIdx`

    def format_edge(self, startPieceIdx, endPieceIdx, startLaneIdx, endLaneIdx):
        return ('p'+`startPieceIdx`+'l'+`startLaneIdx`,
                'p'+`endPieceIdx`+'l'+`endLaneIdx`)

    def get_lane_radius(self, trackPiece, lane):
        radius       = trackPiece['radius']
        distFromCntr = lane['distanceFromCenter']
        if trackPiece['angle'] > 0:
            radius = radius - distFromCntr
        else:
            radius = radius + distFromCntr
        return radius

    def get_track_graph_starting_at(self, currentNode, lookAhead, currentLookAhead):
        if currentLookAhead >= lookAhead:
            return
        for neighbor in self.trackGraph.neighbors(currentNode):
            if self.subGraph.has_node(neighbor):
                edge = (currentNode, neighbor)
                self.subGraph.add_edge(edge, self.trackGraph.edge_weight(edge))

            else:
                self.subGraph.add_node(neighbor)
                edge = (currentNode, neighbor)
                self.subGraph.add_edge(edge, self.trackGraph.edge_weight(edge))
                self.get_track_graph_starting_at(neighbor, lookAhead, currentLookAhead+1)

    def get_track_graph_recursively_from(self, pieceIdx, laneIdx, lookAhead):
        self.subGraph = digraph()
        currentLookAhead = 0
        startNode = 'p'+`pieceIdx`+'l'+`laneIdx`
        self.subGraph.add_node(startNode)
        self.get_track_graph_starting_at(startNode, lookAhead, currentLookAhead)
        return self.subGraph