
class LaneSwitcher(object):

    def __init__(self, raceTrack):
        self.raceTrack = raceTrack
        self.laneOffsets = self.get_lane_offsets()
        self.bestLanes = []
        self.trackPieceSectionIndex = [0] * len(self.raceTrack)

    def get_best_lane_indexes(self):
        print('LaneSwitcher: Overwrite get_best_lane_indexes method')

    def get_lane_offsets(self):
        laneOffsets = map(lambda lane: lane['distanceFromCenter'],
                          self.raceTrack.track_lanes)
        return laneOffsets

    def track_piece_section_index(self, pieceIndex):
        print('LaneSwitcher: Overwrite track_piece_section_indexes method')

