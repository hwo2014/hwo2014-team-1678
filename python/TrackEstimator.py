﻿import collections

# Car drifting angle estimation:
# y: estimated drifting angle, sigma
# x: angular speed, omega
# y[n] = A*x[n] + B*y[n-1] + C*y[n-2]

B =  1.9130
C = -0.9209

# Optimal value seems to depend on track/server physics.
# Increase to overestimate --> increase safety margin.
# Track physics adaptation should reset this value based on some measurements.
A =  0.085

FILTER_DEPTH = 3

class TrackEstimator(object):

    def __init__(self):
        self.resetFilter()

    def resetFilter(self):
        self.sigmas = collections.deque(FILTER_DEPTH*(0.0, ), maxlen=FILTER_DEPTH)

    def update(self, omega):
        s = A*omega + B*self.sigmas[-1] + C*self.sigmas[-2]
        self.sigmas.append(s)
        return s

    def getSigma(self):
        return self.sigmas[-1]
