""" Module for storing race track information. """

import math
from RaceTrackGraph import RaceTrackGraph


def _curve(radius):
    """ Convert radius (-1 or 0...inf) to 'curve' (0.0 ... 100.0).
    - 0 for straight line (inf or negative radius)
    - MAX_CURVE for infinitely sharp curve (0 radius)
    """
    max_curve = 100
    # Adjust how quickly curve drops towards zero as curve increases
    # (lower -> faster drop).
    radius_scaler = 100
    if radius < 0:
        return 0
    return (max_curve - 2 * max_curve * math.atan(radius / radius_scaler) /
            math.pi)


class RaceTrack(object):
    """
    Class for storing race track information.

    Attributes:
        game_init_msg: Dictionary storing race info.
    """
    def __init__(self, game_init_msg):
        self.track_pieces = self._parse_track_pieces(game_init_msg)
        self.track_lanes = self._parse_track_lanes(game_init_msg)
        self.number_of_track_pieces = len(self.track_pieces)

    def __len__(self):
        """ Number of pieces in the track. """
        return self.number_of_track_pieces

    def _parse_track_pieces(self, game_init_msg):
        """ Parse individual track pieces from the init message.

        Returns:
            A list containing the individual track pieces.
        """
        track_info = game_init_msg['race']['track']['pieces']
        return map(self.parse_one_piece, track_info)

    def _parse_track_lanes(self, game_init_msg):
        lane_info = game_init_msg['race']['track']['lanes']
        return map(self.parse_one_lane, lane_info)

    @staticmethod
    def parse_one_piece(piece):
        """ Parse individual track piece data.

        Args:
            piece: Dictioary storing piece information.

        Returns:
            Piece info inside a dictionary structure.
        """
        piece = {
            'length': float(piece.get('length', -1)),
            'switch': piece.get('switch', False),
            'radius': float(piece.get('radius', -1)),
            'angle':  float(piece.get('angle', 0))}
        if piece['length'] < 0:
            # Arc length
            piece['length'] = abs(piece['radius'] * math.pi * piece['angle'] /
                                  180.0)
        return piece

    @staticmethod
    def parse_one_lane(track_lane):
        return {'distanceFromCenter': track_lane.get('distanceFromCenter', -1),
                'index': track_lane.get('index', -1)}

    def track_piece_at_index(self, piece_index):
        return self.track_pieces[piece_index % self.number_of_track_pieces]

    def number_of_lanes(self):
        return len(self.track_lanes)

    def number_of_track_sections(self):
        # Assumption is that there is at least one switch piece per track
        section_index = 0
        for piece in self.track_pieces:
            if piece.get('switch', False):
                section_index += 1
        return section_index

    def all_track_pieces(self):
        return self.track_pieces

    def lane_distances(self):
        laneDistances = {}
        for lane in self.track_lanes:
            index    = lane['index']
            distance = lane['distanceFromCenter']
            laneDistances[index] = distance
        return laneDistances