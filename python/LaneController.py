
from LaneSwitcherSimple import LaneSwitcherSimple

NO_LANE_SWITCH_BEFORE_TICK = 15

class LaneController():
    def __init__(self, raceTrack):
        self.raceTrack    = raceTrack
        self.laneSwitcher = LaneSwitcherSimple(raceTrack)
        self.laneSwitchSent = False

    # Public method
    def switch_lane_now(self, gameTick, carPositionMsg):
        switchLane, switchTo = 0, ''
        if gameTick > NO_LANE_SWITCH_BEFORE_TICK:
            if self.stay_on_current_lane(carPositionMsg):
                self.laneSwitchSent = False
            else:
                if not self.is_lane_switching_ongoing(carPositionMsg):
                    switchLane, switchTo = self.get_lane_switching_parameters(carPositionMsg)
                    self.laneSwitchSent = True
        return switchLane, switchTo

    # Private methods
    def is_lane_switching_ongoing(self, carPositionMsg):
        currentLane = self.get_current_lane(carPositionMsg)
        nextLane    = self.get_next_lane(carPositionMsg)
        return currentLane != nextLane

    def stay_on_current_lane(self, carPositionMsg):
        currentLane = self.get_current_lane(carPositionMsg)
        desiredLane = self.get_desired_lane(carPositionMsg)
        return currentLane == desiredLane

    def get_current_section_index(self, carPositionMsg):
        pieceIndex = carPositionMsg['piecePosition']['pieceIndex']
        return self.laneSwitcher.track_piece_section_index(pieceIndex)

    def get_current_lane(self, carPositionMsg):
        return carPositionMsg['piecePosition']['lane']['startLaneIndex']

    def get_next_lane(self, carPositionMsg):
        return carPositionMsg['piecePosition']['lane']['endLaneIndex']

    def get_desired_lane(self, carPositionMsg):
        lookAhead = 1
        sectionIndex = self.get_current_section_index(carPositionMsg)
        desiredLane = self.laneSwitcher.get_best_lane_for_section(sectionIndex + lookAhead)
        return desiredLane

    def convert_lane_delta_to_word(self, deltaDesiredToCurrent):
        if deltaDesiredToCurrent < 0:
            return 'Left'
        elif deltaDesiredToCurrent > 0:
            return 'Right'
        else:
            return ''

    def get_lane_switching_parameters(self, carPositionMsg):
        switchLane = 0
        if not self.laneSwitchSent:
            currentLane = self.get_current_lane(carPositionMsg)
            desiredLane = self.get_desired_lane(carPositionMsg)
            if desiredLane != currentLane:
                switchLane = desiredLane - currentLane
                self.laneSwitchSent = True
            else:
                self.laneSwitchSent = False
        return switchLane, self.convert_lane_delta_to_word(switchLane)
